import React from 'react'

export default function Card({ article }) {
  return (
    <div className="card">
      <h1>{article.name}</h1>
      <p>{article.content}</p>
    </div>
  )
}
