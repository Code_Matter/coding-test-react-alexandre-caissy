// Exemple pour rendre un component disponible :
// import Component from './Component';
// export { Component }

import Card from './Card'
export { Card }
// Dans les autres fichiers :
// import { Component } from 'components';
