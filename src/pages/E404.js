import React, { Component, Fragment } from "react";
import Helmet from "react-helmet";
import { Link } from "react-router-dom";

import { title } from "utils";
class E404Page extends Component {
  render() {
    const bubbles = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    return (
      <Fragment>
        <Helmet>{title("Page introuvable")}</Helmet>

        {bubbles.map((bubble, idx) => {
          const number = Math.floor(Math.random() * 400);
          const top = Math.floor(Math.random() * window.innerHeight - 500);
          const left = Math.floor(Math.random() * window.innerWidth - 500);
          return (
            <div
              key={idx}
              style={{
                zIndex: "-1",
                width: `${number}px`,
                height: `${number}px`,
                backgroundImage:
                  left > window.innerWidth / 2
                    ? "linear-gradient(to bottom right, white, 30%, #4d75f8)"
                    : "linear-gradient(to top right, white, 30%, #4d75f8)",
                position: "absolute",
                top: top,
                left: left,
                borderRadius: "100%",
              }}
            ></div>
          );
        })}
        <div className="error-page content-wrap">
          <h1>Page introuvable</h1>
          <p>Cette page ne semble pas exister ...</p>
          <Link to="/">Retour à l'accueil</Link>
        </div>
      </Fragment>
    );
  }
}

export default E404Page;
