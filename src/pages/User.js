import React, { Component, Fragment } from "react";
import Helmet from "react-helmet";
import { Link } from "react-router-dom";
import Icon from "@material-ui/core/Icon";
import "moment/locale/fr";
import { history, title } from "utils";

import { userService } from "services";
import moment from "moment";
import { Card } from "components";

class UserPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      id: 1,
      user: {},
    };

    this.selectChangeHandler = (event) => {
      userService
        .get(
          this.state.list.find((user) => {
            return user.name === event.target.value;
          }).id
        )
        .then((res) => {
          this.setState({ ...this.state, user: res });
          history.push(`/users/${this.state.user.id}`)
        })
        .catch((error) => alert(error));
    };
  }
  componentDidMount() {
    moment.locale("fr");
    userService
      .list()
      .then((res) => {
        this.setState({ ...this.state, list: res, id: res[0].id });
      })
      .then(() => {
        userService.get(this.state.id).then((res) => {
          this.setState({ ...this.state, user: res });
        });
      })
      .catch((error) => alert(error));
  }

  render() {
    return (
      <Fragment>
        <Helmet>{title("Page secondaire")}</Helmet>

        <div className="user-page content-wrap">
          <Link to="/" className="nav-arrow">
            <Icon style={{ transform: "rotate(180deg)" }}>arrow_right_alt</Icon>
          </Link>

          {this.state.user && (
            <div className="users-info">
              {this.state.list.length > 0 ? (
                <div className="users-select">
                  <h1>
                    <select onChange={this.selectChangeHandler.bind(this)}>
                      {this.state.list.map((user) => (
                        <option value={user.name} key={`user-${user.id}`}>
                          {user.name}
                        </option>
                      ))}
                    </select>
                    <Icon className="drop-down-icon">arrow_drop_down</Icon>
                  </h1>
                </div>
              ) : <p className="loading">En cours de téléchargement..</p>}

              {(this.state.user.occupation || this.state.user.birthdate) && (
                <div className="infos-block">
                  {this.state.user.occupation && (
                    <p className="occupation">{this.state.user.occupation}</p>
                  )}
                  {this.state.user.birthdate && (
                    <p className="dob">
                      {moment(this.state.user.birthdate).format("DD MMMM YYYY")}
                    </p>
                  )}
                </div>
              )}

              {this.state.user.articles && (
                <div className="articles-list">
                  {this.state.user.articles.map((article) => {
                    return (
                      <Card article={article} key={article.id} />
                    );
                  })}
                </div>
              )}
            </div>
          )}
        </div>
      </Fragment>
    );
  }
}

export default UserPage;